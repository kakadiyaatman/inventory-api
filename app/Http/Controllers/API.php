<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Items;

class API extends Controller
{
	public function items(Request $request)
    {
   		try{
   			$i=Items::orderBy("item_id","desc")->get();
   			if($i->count()>0){
				return response()->json(["status"=>1,"message"=>"Successful get items.","data"=>$i],200);
			}else{
				return response()->json(["status"=>0,"message"=>"Items not found."],400);
			} 		
   		}
		catch(Exception $e) {
			return response()->json(["status"=>0,"message"=>"Error Message : ".$e->getMessage()],400);	
  		}
    }
    public function itemEdit(Request $request)
    {
   		try{
   			$item_id=$request->input('item_id');
   			$selected=$request->input('selected');
   			$i=Items::find($item_id);
   			$i->selected=$selected;
   			if($i->save()){
				return response()->json(["status"=>1,"message"=>"Successful Item Edited."],200);
			}else{
			 	return response()->json(["status"=>0,"message"=>"Try again later."],400);
			} 		
   		}
		catch(Exception $e) {
			return response()->json(["status"=>0,"message"=>"Error Message : ".$e->getMessage()],400);	
  		}
    }
    public function itemInsert(Request $request)
    {
   		try{
   			$name=$request->input('name');
   			if(!empty($name)){
				$get_dp=Items::where("name",$name)->get();
	   			if($get_dp->count()<=0){
	   				$i=new Items();
	   				$i->name=$name;
	   				if($i->save()){
						return response()->json(["status"=>1,"message"=>"Successful Item Added."],200);
					}else{
				 		return response()->json(["status"=>0,"message"=>"Try again later."],400);
					} 		
	   			}else{
				 	return response()->json(["status"=>0,"message"=>"Already Item Existed."],400);
	   			}
	   		}else{
	 			return response()->json(["status"=>0,"message"=>"Please Enter Name."],400);
	   		}
  		}
		catch(Exception $e) {
			return response()->json(["status"=>0,"message"=>"Error Message : ".$e->getMessage()],400);	
  		}
    }
}
